# coding: utf-8
from argparse import ArgumentParser
from bs4 import BeautifulSoup
from codecs import getwriter
import json
import requests
from requests.adapters import HTTPAdapter
from signal import signal, SIGINT, SIGTERM
from sys import stdout, exit, exc_info, stderr
import threading
import Queue

TIMEOUT = 90
MAX_RETRIES = 20
ALIVE = True
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}


def clean(val):
    return val.strip()


class BaseParser(object):
    def __init__(self, page_url, **kwargs):
        self._session = requests.Session()
        self._session.headers.update(headers)
        self._session.mount(page_url, HTTPAdapter(max_retries=MAX_RETRIES))
        self._url = page_url
        self._BASE = kwargs


class Page(BaseParser):
    def get_result(self):
        result = []
        response = self._session.get(self._url)
        soup = BeautifulSoup(response.content, 'html.parser')
        for tr in soup.find('div', attrs={'id': 'main-content'}).find('tbody').findAll('tr'):
            try:
                recipe = {
                    'url': self._BASE['url']+clean(tr.findAll('td')[1].find('a').get('href'))[1:],
                    'name': clean(tr.findAll('td')[1].text)
                }
                result.append(recipe)
            except:
                pass
        return result


class Recipe(BaseParser):
    CONTEXT = {
        u'Во время Поста',
        u'Завтрак',
        u'Обед',
        u'Ужин',
        u'Пикник',
        u'Перекус',
        u'Праздник',
    }
    def set_value(self, recipe, method, html):
        key, val = self.call_method(method, html)
        if val:
            recipe[key] = val
        return recipe

    def call_method(self, method, html):
        try:
            return getattr(self, method)(html)
        except:
            return (None, None, )

    def get_type(self, html):
        breadcrumb = html.find('div', attrs={'id': 'breadcrumb'}).find('div', attrs={'class': 'breadcrumb'}).findAll('a')[-2]
        return 'type', [clean(breadcrumb.text)]

    def get_context(self, html):
        context = html.find('div', attrs={'id': 'recipes-col2'}).findAll('a')
        return 'context', filter(lambda x: x in self.CONTEXT, map(lambda x: clean(x.text), context))

    def get_tags(self, html):
        context = html.find('div', attrs={'id': 'recipes-col2'}).findAll('a')
        return 'tags', filter(lambda x: x not in self.CONTEXT, map(lambda x: clean(x.text), context))

    def get_images(self, html):
        images = []
        img = html.find('div', attrs={'class': 'field field-type-filefield field-field-picture'}).find('img')
        images.append(img.get('src'))
        return 'images', images

    def get_description(self, html):
        desc = html.find('div', attrs={'class': 'node-content'}).findAll('p')[0]
        return 'description', clean(desc.text)

    def get_cooking_time(self, html):
        icons = [154, 155, 156, 157, 158]
        for ic in icons:
            cooking_time = html.find('div', attrs={'id': 'recipes-col2'}).find('div', attrs={'class': 'recipes-icon-%i'%ic})
            if cooking_time is not None:
                return 'cooking_time', clean(cooking_time.get('title'))
        raise Exception

    def get_number_of_servings(self, html):
        number_of_servings = html.find('span', attrs={'itemprop': 'recipeYield'})
        return 'number_of_servings', clean(number_of_servings.text)

    def get_ingredients(self, html):
        table = html.find('table', attrs={'id': 'ar_tabl'})
        ingredients = []
        for tr in table.find('tbody').findAll('tr'):
            data = {
                'name': [clean(tr.findAll('td')[0].text)],
                'quantity': clean(tr.findAll('td')[1].text),
            }
            data['full'] = u'%s - %s' % (data['name'][0], data['quantity'])
            ingredients.append(data)
        return 'ingredients', ingredients

    def get_step_by_step_recipe(self, html):
        steps = []
        for img in html.find('div', attrs={'id': 'galleria'}).findAll('img'):
            steps.append({
                'description': clean(img.get('alt')),
                'images': [clean(img.get('src')), clean(img.get('src').replace('recipes_preview', 'recipes_full'))],
            })
        if not steps:
            for li in html.find('div', attrs={'itemprop': 'recipeInstructions'}).findAll('li'):
                steps.append({
                    'description': clean(li.text),
                })
        return 'step_by_step_recipe', steps

    def get_result(self):
        recipe = self._BASE.get('recipe')
        response = self._session.get(self._url)
        soup = BeautifulSoup(response.content, 'html.parser')
        recipe = self.set_value(recipe, 'get_type', soup)

        main_content = soup.find('div', attrs={'id': 'main-content'})
        methods = {'get_tags', 'get_context', 'get_images', 'get_description', 'get_cooking_time', 'get_number_of_servings', 'get_ingredients', 'get_step_by_step_recipe'}
        for method in methods:
            recipe = self.set_value(recipe, method, main_content)
        return recipe


class Client(object):
    THREADS = 32

    def __init__(self, sout, BASE):
        self._BASE = BASE
        self.sout = sout
        self._session = requests.Session()
        self._session.headers.update(headers)
        self._session.mount(self._BASE['url'], HTTPAdapter(max_retries=MAX_RETRIES))
        self._queue = []
        response = self._session.get(self._BASE['url'])
        soup = BeautifulSoup(response.content, 'html.parser')
        for li in soup.find('div', attrs={'id': 'sidebar-left'}).find('ul', attrs={'class': 'menu'}).findAll('li'):
            if li.find('li') is not None:
                continue
            a = li.find('a')
            if a.get('href').find('recipe') == -1 or a.get('href').find('analyzer') > -1:
                continue
            self._queue.append(self._BASE['url']+a.get('href')[1:])

    def _run_cats(self):
        raw_data = Queue.Queue()
        for url in self._queue:
            self._run_cat(url, raw_data)
        return raw_data

    def _run_cat(self, url, raw_data):
        response = self._session.get(url)
        soup = BeautifulSoup(response.content, 'html.parser')
        try:
            count_pages = int(soup.find('div', attrs={'id': 'main-content'}).find('ul', attrs={'class': 'pager'}).findAll('li')[-2].text)
        except Exception as e:
            count_pages = 1
        threads = []
        work_q = Queue.Queue()
        for i in range(count_pages):
            work_q.put(url+'?page=%i'%i)

        def mult_process(work_q, raw_data):
            while ALIVE:
                if work_q.empty():
                    break
                page_url = work_q.get()
                try:
                    res = Page(page_url, **self._BASE).get_result()
                    for recipe in res:
                        raw_data.put(recipe)
                except Exception as e:
                    pass

        thread_count = self.THREADS if self.THREADS < count_pages else count_pages
        for x in range(thread_count):
            threads.append(threading.Thread(target=mult_process, args=(work_q, raw_data, )))
        for t in threads:
            t.start()
        for t in threads:
            t.join()

    def process(self):
        recipes = self._run_cats()
        work_q = Queue.Queue()
        d = {}
        while not recipes.empty():
            x = recipes.get_nowait()
            d[x['url']] = x['name']
        for url, name in d.items():
            work_q.put({'url': url, 'name': name})

        def mult_process(work_q):
            while ALIVE:
                if work_q.empty():
                    break
                recipe = work_q.get()
                recipe = Recipe(recipe['url'], recipe=recipe, **self._BASE).get_result()
                self.sout.write(json.dumps(recipe, ensure_ascii=False) + "\n")

        threads = []
        thread_count = self.THREADS if self.THREADS < work_q.qsize() else work_q.qsize()
        for x in range(thread_count):
            threads.append(threading.Thread(target=mult_process, args=(work_q, )))
        for t in threads:
            t.start()
        for t in threads:
            t.join()


def main():
    sout = getwriter("utf8")(stdout) 
    serr = getwriter("utf8")(stderr)
    URLS = {
        'calorizator': {
            'url': 'http://www.calorizator.ru/',
        },
    }
    client = Client(sout, URLS['calorizator'])
    
    try:
        client.process()
    except Exception as e:
        exc_traceback = exc_info()[2]
        filename = line = None
        while exc_traceback is not None:
            f = exc_traceback.tb_frame
            line = exc_traceback.tb_lineno
            filename = f.f_code.co_filename
            exc_traceback = exc_traceback.tb_next
        serr.write(json.dumps({
            'error': True,
            'details': {
                'message': str(e),
                'file': filename,
                'line': line
            }
        }, ensure_ascii=False) + "\n")


if __name__ == "__main__": 
    def signal_handler(signal, frame):
        global ALIVE
        ALIVE = False
        exit(0)
    signal(SIGINT, signal_handler)
    signal(SIGTERM, signal_handler)
    main() 
